class Person:
    def __init__(self,name,age,address,cellphone):
        self.__name = name
        self.__age = age
        self.__address = address
        self.__cellphone = cellphone
    def get_name(self):
        return self.__name
    def get_age(self):
        return self.__age
    def get_adress(self):
        return self.__address
    def get_cellphone(self):
        return self.__cellphone
    def set_name(self,new_name):
        self.__name = new_name
    def set_age(self,new_age):
        self.__age = new_age
    def set_address(self,new_address):
        self.__address = new_address
    def set_cellphone(self,new_cellphone):
        self.__cellphone = new_cellphone

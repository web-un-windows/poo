import Person
class Patient(Person):
    def __init__(self, name, age, address, cellphone,healthInsurance,history,idPatient):
        super().__init__(name, age, address, cellphone)
        self.__healthInsurance = healthInsurance
        self.__history = history
        self.__idPatient = idPatient
    def get_healthInsurance(self):
        return self.__healthInsurance
    def get_idPatient(self):
        return self.__idPatient
    def set_idPatient(self,new_idPatient):
        self.__idPatient = new_idPatient
    def get_history(self):
        return self.__history
    def set_healthInsuranc(self,new_Insurance):
        self.__healthInsurance = new_Insurance
    def set_history(self,new_history):
        self.__history = new_history
